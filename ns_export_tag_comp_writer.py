# *******************************************************
# * Copyright (C) 2020 Hendrik Proosa
# * Contact: hendrik [at] kalderafx.com
# * 
# * This script can be copied and/or distributed without
# * the express permission of Hendrik Proosa, but this
# * copyright notice must be included.
# *******************************************************
# Use with Python 2.7 in Nuke Studio
# Tested in Nuke Studio version 12.0

from hiero.exporters.FnShotProcessor import ShotProcessorPreset

def shot_addUserResolveEntries(self, resolver):

    def task_shot_comp_writer(task):
        # Task is executed for each (selected) timeline item, lets get the item
        trackItem = task._item

        # Get trackitem name for default return value
        comp_write_name = trackItem.name()

        # If track item has this specific metadata key, which points to .nk script write, read the value
        if trackItem.source().mediaSource().metadata().hasKey('media.nk.writepath'):
            comp_write_name = trackItem.source().mediaSource().metadata().value('media.nk.writepath')
            comp_write_name = comp_write_name.split('/')[-1]
            nameParts = comp_write_name.split('.')[0:-1]
            comp_write_name = '.'.join(nameParts)
        
        return comp_write_name

    resolver.addResolver("{comp_writer}", "File name from comp Write node", lambda keyword, task: task_shot_comp_writer(task))

# This token only applies to the Shot Processor as it references the shotname
ShotProcessorPreset.addUserResolveEntries = shot_addUserResolveEntries
